import React from 'react';
import MapView,{ Marker } from 'react-native-maps';

const MyMapView = (props) => {
    console.log(props.region)
    return (
        <MapView
            style={{ flex: 1 }}
          region={props.region}
             
           initialRegion={{
            latitude: props.region.latitude,
            longitude: props.region.longitude,
            latitudeDelta: 0.03,
            longitudeDelta: 0.03
          }} 
            showsUserLocation={true}
            onRegionChange={(reg) => props.onRegionChange(reg)}>
         

        <Marker coordinate={props.region} />
        </MapView>
    )
}
export default MyMapView;