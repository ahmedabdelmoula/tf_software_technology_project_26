import {
    LOADING_OBJECTS, LOADING_OBJECT_SUCCESS ,LOADING_OBJECT_FAILED
    } from './types';
    
    import { AsyncStorage } from 'react-native';
    import axios from 'axios';
    
  
  export function deletePost(key){
    return (dispatch) => {
  
      AsyncStorage.getItem('app_token').then(token => {
  
        fetch('http://192.168.1.10:5000/post/delete/'+key, {
          method: 'DELETE',
          mode: 'cors',
          headers: new Headers({
            "Authorization": 'Bearer ' + token,
            'Content-Type': 'application/json',
            //'Accept': 'application/json'
    
          })
    
        })
          .then(response => { return response.json(); })
        //  .then(responseData => { return responseData; })
          .then(responseData => {
            if (responseData.success) {  dispatch({ type: LOADING_OBJECT_SUCCESS, objects: responseData.objects}) }
            else
            dispatch({ type: LOADING_OBJECT_FAILED, error: responseData.message })
          })
    
          .catch(err => {
            console.log("fetch error" + err);
          });
      })
    }
  }
    export const fetchObjects= () => {
      return (dispatch) => {
        dispatch({ type: LOADING_OBJECTS });
    
        AsyncStorage.getItem('app_token').then(token => {
  
          fetch('http://192.168.1.10:5000/objects/all', {
            method: 'GET',
            mode: 'cors',
            headers: new Headers({
              "Authorization": 'Bearer ' + token,
              'Content-Type': 'application/json',
              //'Accept': 'application/json'
      
            })
      
          })
            .then(response => { return response.json(); })
          //  .then(responseData => { return responseData; })
            .then(responseData => {
              if (responseData.success) {  dispatch({ type: LOADING_OBJECT_SUCCESS, objects: responseData.objects }) }
              else
              dispatch({ type: LOADING_OBJECT_FAILED, error: responseData.message })
            })
      
            .catch(err => {
              console.log("fetch error" + err);
            });
        })
      
        
      }
    }
  
  
  
    
  