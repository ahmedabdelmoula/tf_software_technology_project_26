import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet, Image } from "react-native";
import { fetchGrade } from "../../actions/grade.action";
import { connect } from "react-redux";
import * as Progress from "react-native-progress";
import SvgUri from "react-native-svg-uri";

class ListGrade extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gradeList: [],
    };
  }
  renderPost = (item,index) => {
  

    return (
      <View key={`grade${index}`} style={{ flexDirection: "row", justifyContent: "center" ,alignItems:'center'}}>
        {this.props.gradeUser._id == item._id ? (
          <SvgUri

            width="25"
            height="25"
            source={{
              uri: "https://image.flaticon.com/icons/svg/1329/1329665.svg",
            }}
          />
        ) : (
          <Text>{""}</Text>
        )}

        <Text>{item.gradeName + "   "}</Text>
        <Text>{item.lowPoint}</Text>
        <View>
          <Progress.Bar
            progress={
              this.props.gradeUser._id < item._id
                ? 0
                : this.props.gradeUser._id > item._id
                ? 1
                : (this.props.score - this.props.gradeUser.lowPoint) /
                  (item.highPoint - item.lowPoint)
            }
            width={156}
          />
        </View>
        <Text>{item.highPoint}</Text>
      </View>
    );
  };
  componentDidMount() {
    this.props.fetchGrade();
    setTimeout(() => this.setState({ gradeList: this.props.grades }), 3000);
  }

  render() {
    return (
      <View>
        {!(
          Array.isArray(this.state.gradeList) && this.state.gradeList.length
        ) ? (
          <Text style={styles.load}>Loading Please Wait</Text>
        ) : (
          <FlatList
            style={styles.feed}
            data={this.state.gradeList}
            renderItem={({ item ,index }) => this.renderPost(item,index)}
            keyExtractor={(item, index) => {
              return index.toString();
            }}
            showsVerticalScrollIndicator={false}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  feed: {
    marginHorizontal: 16,
  },
  load: {
    paddingTop: 20,
    color: "#C4C6CE",
    fontSize: 11,
  }
});
const mapStateToProps = (state) => {
  return {
    grades: state.grades.grades,
  };
};
export default connect(mapStateToProps, {
  fetchGrade,
})(ListGrade);
