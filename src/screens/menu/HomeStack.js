import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import PostScreen from '../Post/PostScreen';
import Found from '../Post/Found';
import { Header } from '../../components';

const navOptionHandler = () => ({
    headerShown: false,
  });
const StackHome = createStackNavigator();
const TabTop = createMaterialTopTabNavigator();
 const TopTab = ({ navigation, route }) => {

// function TopTab() {
  return (
    <TabTop.Navigator
    
    tabBarOptions={{
      labelStyle: {
       fontWeight:"bold"
      }
    }}
    
    >
      <TabTop.Screen
        name="Lost"
        component={PostScreen}
        options={{
          // tabBarLabel: "Lost",
          tabBarLabel: "Q&A",

          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="message-image"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <TabTop.Screen
        name="Found"
        component={Found}
        options={{
          // tabBarLabel: "Found",
          tabBarLabel: "LATEST NEWS",

          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="message-image"
              color={color}
              size={size}
            />
          ),
        }}
      />
    </TabTop.Navigator>
  );
}


export const HomeStack = ({ navigation, route }) => {

  
    if (
      route.state &&
      route.state.routeNames[route.state.index] === "Home"
    ) {
      navigation.setOptions({ headerShown: false })

    } else {
      navigation.setOptions({ headerShown: false })
    }
    return (
      <StackHome.Navigator initialRouteName="Home" >
        <StackHome.Screen
          name="Home"
          component={TopTab}
          options={{

            headerShown: false,

            header: ({ navigation, scene }) => (
              <Header
                title="Home"
                search
                options
                navigation={navigation}
                scene={scene}
              />
            ),
            cardStyle: { backgroundColor: "#F8F9FE" },
          }} 
        />
      </StackHome.Navigator>
    );
  };