import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import auth from '../reducers/auth.reducer';
import post from '../reducers/post.reducer';
import object from '../reducers/object.reducer'
import postsListReducer from '../reducers/postsList.reducer';
import user from '../reducers/user.reducer'
import objects from '../reducers/ObjectsList.reducer'
const rootReducer = combineReducers({
	auth,
	post,
	object,
	objects,
	user,
	postsList:postsListReducer,	
});

export default rootReducer;
