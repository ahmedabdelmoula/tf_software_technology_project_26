import React from "react";
import moment from "moment";
import { Button } from "../../components";
import {
  ScrollView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  TouchableOpacity,
  View,TouchableHighlight
} from "react-native";
import { Block, Text, theme } from "galio-framework";
import { Ionicons } from "@expo/vector-icons";
import { argonTheme } from "../../constants/";
import Input from "../../components/Input";
import { deletePost, editPost } from "../../actions/post.actions";
import { connect } from "react-redux";
import { SCLAlert, SCLAlertButton } from "react-native-scl-alert";
import {
  MaterialCommunityIcons,
  FontAwesome,
} from "@expo/vector-icons";
import SwitchSelector from "react-native-switch-selector";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { like, dislike, sendMessage, deleteComment } from "../../actions/comment.like.actions";
import Adresse from "./Adresse";
import Swipeable from 'react-native-swipeable';



const { width } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;
const cardWidth = width - theme.SIZES.BASE * 2;

const options = ["Cancel", "Edit", "Delete"];
const options2 = ["Cancel", "Signalize"];
const data = [
  { id: 1, label: "#undesirable content" },
  { id: 2, label: "#Fake profile" },
  { id: 3, label: "#Violence" },
  { id: 4, label: "#Not Real " },
  { id: 5, label: "#Steal" },
];
class PostDetails extends React.Component {
  state = {
    message: "",
    showMessages: false,
    showlikes: false,
    show: false,
    showEdit: false,
    title: "",
    description: "",
    reward: false,
      leftActionActivated: false,
      toggle: false,
      commentsList:[]
 
  };
  

  componentDidMount() {
    this.setState({ title: this.props.route.params.post.title });
    this.setState({ description: this.props.route.params.post.description });
    this.setState({ reward: this.props.route.params.post.reward });
    this.setState({ commentsList: this.props.route.params.post.comments });


        const {navigation} = this.props;
    navigation.addListener ('focus',  () =>{
      this.setState({ commentsList: this.props.route.params.post.comments });
      // console.log(this.state.commentsList,"fi wost compantd did mount")

    });  
  }
  handleOpen = () => {
    this.setState({ show: true });
  };
  handleDelete = (post) => {
    this.props.deletePost(post._id);
    // this._onRefreshTasks();
    this.setState({ show: false });
  };
  handleEdit = (post) => {
    var id = post._id;
    const { title, description, reward } = this.state;
    this.props.editPost(title, description, reward, post);
    //this._onRefreshTasks();
    this.setState({ showEdit: false });
  };
  handleClose = () => {
    this.setState({ show: false });
    this.setState({ showEdit: false });
  };
  showActionSheet = () => {
    this.ActionSheet.show();
    console.log('helo')
  };
  like = (post) => {
    console.log(" Mcha lel like");
    this.props.like(post._id);
  };

  dislike = (post) => {
    console.log(" Mcha lel dislike");
    this.props.dislike(post._id);
  };

  onWriteComment = (text) => {
    console.log(text.nativeEvent.text);
    this.setState({
      message: text.nativeEvent.text,
    });
  };
  showMessages = () => {
    this.setState({
      showMessages: !this.state.showMessages,
    });
  };
  showlikes = () => {
    this.setState({
      showlikes: !this.state.showlikes,
    });
  };
  renderIA = (author, user) => {
    if (user == author) {
      return (
        <Ionicons
          name="ios-more"
          size={24}
          color="#737888"
          style={{ paddingLeft: 270 }}
          onPress={this.showActionSheet}
        />
      );
    } else
      return (
        <Ionicons
          name="ios-more"
          size={24}
          color="#737888"
          style={{ paddingLeft: 270 }}
          onPress={this.showActionSheet2}
        />
      );
  };

  renderLikeCommentsNumber = (post) => {
    if (!this.state.showlikes || !this.state.showMessages) {
      return (
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            onPress={this.showlikes.bind(this)}
            style={{ marginLeft: 2 }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textSeeComments}>
                {post.likes_number} likes |
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.showMessages.bind(this)}
            style={{ marginLeft: 8 }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textSeeComments}>
                {this.state.commentsList.length} comments
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={this.showMessages.bind(this)}
          style={{ marginLeft: 15 }}
        >
          <View>
            <Text style={styles.textSeeComments}>Close comments</Text>
          </View>
        </TouchableOpacity>
      );
    }
  };

  renderAction = (author, user) => {
    if (user == author)
      return (
        <ActionSheet
          ref={(o) => (this.ActionSheet = o)}
          title={
            <Text style={{ color: "#000", fontSize: 18 }}>Choose Option ?</Text>
          }
          options={options}
          cancelButtonIndex={0}
          destructiveButtonIndex={4}
          //  onPress={this.handlePress2}    this.handleOpen
          onPress={(index) => {
            console.log(index);
            if (index == 2)
              setTimeout(() => this.setState({ show: true }), 500);
            else if (index == 1)
              setTimeout(() => this.setState({ showEdit: true }), 500);
          }}
        />
      );
    else {
      return (
        <ActionSheet
          ref={(o) => (this.ActionSheet2 = o)}
          title={
            <Text style={{ color: "#000", fontSize: 18 }}>Choose Option ?</Text>
          }
          options={options2}
          cancelButtonIndex={0}
          destructiveButtonIndex={4}
          onPress={(index) => {
            console.log(index);
            if (index == 1)
              setTimeout(() => this.setState({ visibleModal: 1 }), 500);
          }}
        />
      );
    }
  };
  renderSendMessage = (postt) => {
    if (this.state.message.length > 0) {
      return (
        <View style={{ paddingTop: 16, paddingLeft: 10 }}>
          <Button
            onPress={() =>
              this.props.sendMessage(postt._id, this.state.message)
            }
            small
            style={{ backgroundColor: argonTheme.COLORS.INFO }}
          >
            Send
          </Button>
        </View>
      );
    }
  };
  renderIcon = (reward) => {
    if (reward == true) {
      return (
        <View>
          <Ionicons name="ios-gift" size={25} color="#F22B53" />
        </View>
      );
    }
  };
  renderProduct = (post) => {
    const { navigation } = this.props;
    const { title, description, reward } = this.state;
    const option = [
      { label: "Free", value: false },
      { label: "Reward ", value: true },
    ];

    return (
      <TouchableWithoutFeedback
        style={{ zIndex: 3 }}
        key={`product-${this.props.route.params.post.id}`}
      >
        <Block center style={styles.productItem}>
          <Text></Text>
          {this.renderIA(
            this.props.route.params.connectedUser,
            this.props.route.params.post.author._id
          )}

          <Text></Text>
          <Image
            resizeMode="cover"
            style={styles.productImage}
            source={{ uri: this.props.route.params.post.image }}
          />
          {this.renderAction(
            this.props.route.params.connectedUser,
            this.props.route.params.post.author._id
          )}

          <SCLAlert
            show={this.state.show}
            onRequestClose={this.handleClose}
            theme="danger"
            title="Info"
            subtitle="You can setup the colors using the theme prop"
            headerIconComponent={
              <MaterialCommunityIcons name="delete" size={32} color="white" />
            }
          >
            <SCLAlertButton
              theme="danger"
              onPress={() =>
                this.handleDelete(this.props.route.params.post._id)
              }
            >
              Done
            </SCLAlertButton>
            <SCLAlertButton theme="default" onPress={this.handleClose}>
              Cancel
            </SCLAlertButton>
          </SCLAlert>

          <SCLAlert
            show={this.state.showEdit}
            onRequestClose={this.handleClose}
            theme="warning"
            title="Edit"
            subtitle="You can Edit your Post"
            headerIconComponent={
              <FontAwesome name="edit" size={32} color="white" />
            }
          >
            <Input
              style={[styles.input]}
              placeholder="Title"
              defaultValue={title}
              onChangeText={(title) => this.setState({ title })}
            />
            <Input
              style={[styles.input]}
              placeholder="Description"
              //  value
              defaultValue={description}
              onChangeText={(description) => this.setState({ description })}
            />

            <SwitchSelector
              options={option}
              initial={reward ? 1 : 0}
              onPress={(value) => this.setState({ reward: value })}
            />
            <SCLAlertButton
              theme="warning"
              onPress={() => this.handleEdit(this.props.route.params.post)}
            >
              Done
            </SCLAlertButton>
          </SCLAlert>

          <Block center style={{ paddingHorizontal: theme.SIZES.BASE }}>
            <Text center size={34}>
              {title}
            </Text>

            <Text
              center
              size={16}
              color={theme.COLORS.MUTED}
              style={styles.productDescription}
            >
              {description}
            </Text>
            {this.renderIcon(this.props.route.params.post.reward)}
          </Block>
        </Block>
      </TouchableWithoutFeedback>
    );
  };

  renderlikes = (post) => {
    if (post !== undefined) {
      if (this.state.showlikes) {
        return post.likes.map((like, i) => {
         this.props.getUser(like);
       

          return (
            <View style={styles.container2}>
              <TouchableOpacity onPress={() => {}}></TouchableOpacity>
              <View style={styles.content}>
                <View style={styles.contentHeader}>
                  <Text style={styles.name2}>{like}</Text>
                  <Text style={styles.time}></Text>
                </View>

                <View style={styles.separator} />
              </View>
            </View>
          );
        });
      }
    }
  };



  renderComments = (post) => {
    const {leftActionActivated, toggle} = this.state;
    if (post !== undefined) {
      // if (this.state.showMessages) {
      return this.state.commentsList.map((comment, i) => {
        return (
        <Swipeable
          leftActionActivationDistance={200}
          leftContent={(
            <View style={[styles.leftSwipeItem, {backgroundColor: leftActionActivated ? '' : 'red' }] }>
              {leftActionActivated ?
                <Text>release!</Text> :
                <Text>Delete!</Text>}
            </View>
          )}
          onLeftActionActivate={() => this.setState({leftActionActivated: true})}
          onLeftActionDeactivate={() => this.setState({leftActionActivated: false})}
         onLeftActionComplete={() => {

          this.setState({
            commentsList: this.state.commentsList.filter((item, i) => 
            item._id !== comment._id )
          });
          this.props.deleteComment(post._id,comment._id) }}> 
    
          <View style={styles.container2}>
      
              <Image
                style={styles.image}
                source={{ uri: comment.postedBy.avatar }}
              />
            <View style={styles.content}>
              <View style={styles.contentHeader}>
                <Text style={styles.name2}>
                  {comment.postedBy.first_name} {comment.postedBy.last_name}
                </Text>
                <Text style={styles.time}>
                  {moment(comment.created).fromNow()}
                </Text>
              </View>
              <Text rkType="primary3 mediumLine"> {comment.text}</Text>
              <View style={styles.separator} />
            </View>
          </View>
         </Swipeable>
       );
      });
      // }
    }
  };
 

  render() {
    return (
      <KeyboardAwareScrollView>
        <Block flex center>
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.renderProduct(this.props.route.params.post)}
            <View style={{ flexDirection: "row" }}>
              <Text>{"   "}</Text>

              <Ionicons
                name="ios-heart"
                size={24}
                color={
                  this.props.route.params.post.likes.indexOf(
                    this.props.route.params.post.author._id
                  ) === -1
                    ? "#737888"
                    : "red"
                }
              />
              <Text>{"       "}</Text>
              <Ionicons name="ios-chatboxes" size={24} color="#737888" />
            </View>

            <View style={{ alignContent: "center" }}>
              {this.renderLikeCommentsNumber(this.props.route.params.post)}
            </View>
            <View style={{ alignContent: "center" }}>
              {this.renderlikes(this.props.route.params.post)}
            </View>
            <View style={styles.seeComments}>
              {this.renderComments(this.props.route.params.post)}

            </View>
            <Adresse />
          </ScrollView>
        </Block>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    paddingBottom: theme.SIZES.BASE,
    paddingHorizontal: theme.SIZES.BASE * 2,
    marginTop: 22,
    color: argonTheme.COLORS.HEADER,
  },
  group: {
    paddingTop: theme.SIZES.BASE,
  },
  albumThumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: "center",
    width: thumbMeasure,
    height: thumbMeasure,
  },
  category: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE / 2,
    borderWidth: 0,
  },
  categoryTitle: {
    height: "100%",
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  imageBlock: {
    overflow: "hidden",
    borderRadius: 4,
  },
  productItem: {
    width: cardWidth - theme.SIZES.BASE * 2,
    marginHorizontal: theme.SIZES.BASE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 7 },
    shadowRadius: 10,
    shadowOpacity: 0.2,
  },
  productImage: {
    width: cardWidth - theme.SIZES.BASE,
    height: cardWidth - theme.SIZES.BASE,
    borderRadius: 3,
  },
  productPrice: {
    paddingTop: theme.SIZES.BASE,
    paddingBottom: theme.SIZES.BASE / 2,
  },
  productDescription: {
    paddingTop: theme.SIZES.BASE,
    // paddingBottom: theme.SIZES.BASE * 2,
  },
  header: {
    paddingTop: 64,
    paddingBottom: 16,
    backgroundColor: "#FFF",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#EBECF4",
    shadowColor: "#454D65",
    shadowOffset: { height: 5 },
    shadowRadius: 15,
    shadowOpacity: 0.2,
    zIndex: 10,
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: "500",
  },
  feed: {
    marginHorizontal: 16,
  },
  
  leftSwipeItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 20
  },
  feedItem1: {
    alignItems: "center",
    backgroundColor: "#FFF",
    borderRadius: 5,
    padding: 8,
    flexDirection: "row",
    marginVertical: 8,
  },
  feedItem: {
    backgroundColor: "#FFF",
    borderRadius: 5,
    padding: 8,
    flexDirection: "row",
    marginVertical: 8,
  },
  avatar: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginRight: 16,
  },
  name: {
    fontSize: 15,
    fontWeight: "500",
    color: "#454D65",
  },
  timestamp: {
    fontSize: 11,
    color: "#C4C6CE",
    marginTop: 4,
  },
  post: {
    marginTop: 16,
    fontSize: 14,
    color: "#838899",
    alignItems: "center",
  },
  load: {
    paddingTop: 20,
    color: "#C4C6CE",
    fontSize: 11,
  },
  postImage: {
    width: 280,
    height: 200,
    borderRadius: 5,
    alignItems: "center",
    marginVertical: 16,
  },
  root: {
    backgroundColor: "#ffffff",
    marginTop: 10,
  },
  container2: {
    paddingLeft: 19,
    paddingRight: 16,
    paddingVertical: 12,
    flexDirection: "row",
    alignItems: "flex-start",
  },
  content: {
    marginLeft: 16,
    flex: 1,
  },
  contentHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 6,
  },
  ligne: {
    flexDirection: "row",
  },
  separator: {
    height: 1,
    backgroundColor: "#CCCCCC",
  },
  image: {
    width: 40,
    height: 44,
    borderRadius: 20,
    marginLeft: 20,
  },
  time: {
    fontSize: 11,
    color: "#808080",
  },
  name2: {
    fontSize: 16,
    fontWeight: "bold",
  },
  textSeeComments: {
    fontSize: 15,
    color: "grey",
  },
});

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {
  deletePost,
  editPost,
  like,
  dislike,
  deleteComment
})(PostDetails);
