import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { Entypo } from "@expo/vector-icons";
import { gs, colors } from "./styles";
import MapView, { Marker } from "react-native-maps";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";

export default class Adresse extends Component {
  constructor(props) {
    super(props);
    this.state = {

      adresse : ""
    };
  }

  
  componentDidMount () {
    Permissions.askAsync(Permissions.LOCATION);
    setTimeout(() => {
      this.getAdresse()   
     }, 2000);
    
  }
 async getAdresse() {
  var { latitude , longitude } = this.props.location;
    let result = await Location.reverseGeocodeAsync({latitude,longitude});   
            if (result.length > 0) {
              let formatted_address  = result[0].country +" "+ result[0].city +" "+result[0].postalCode;
              result[0].street!=undefined? formatted_address=result[0].street +" "+ result[0].region :formatted_address
              this.setState({ adresse: formatted_address });
            }
  }

  render() {
    return (
      <View>
        <View
          style={{

            borderRadius: 10,

            overflow: "hidden",
     
            backgroundColor: "#000",
          }}
        >
          <MapView
            style={{ flex: 1, height: 200, opacity: 0.4 }}
            itchEnabled={false}
            rotateEnabled={false}
            zoomEnabled={false}
            scrollEnabled={false}

            initialRegion={{
              latitude:this.props.location.latitude,
              longitude: this.props.location.longitude,
              latitudeDelta: 0.03,
              longitudeDelta: 0.03
            }}
          >
            <Marker
              coordinate={{
                latitude:this.props.location.latitude,
                longitude: this.props.location.longitude,
                latitudeDelta: 0.03,
                longitudeDelta: 0.03
              }}
            />
          </MapView>
        </View>
        <View style={styles.addressContainer}>
          <View style={{ marginLeft: 8, marginTop: 24 }}>
            <Text style={gs.sectionTitle}>Address</Text>
            <Text style={styles.address}>{this.state.adresse}</Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  addressContainer: {
    ...gs.absoluteFull,
    flexDirection: "row",
    paddingHorizontal: 32,
    paddingVertical: 16,
  },
  address: {
    ...gs.smallText,
    color: colors.darkHl,
    marginTop: 6,
    letterSpacing: 1,
    lineHeight: 20,
    fontWeight :'bold'
  },
});
