import React, { Component } from 'react';
import { View, Text ,Picker,StyleSheet,TextInput} from 'react-native';
import { addPost } from "../../actions/post.actions";
import { connect } from "react-redux";
import { RadioButton } from "react-native-paper";
import { Root, Popup } from 'popup-ui'
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import Map from '../Post/Map';
import ImageUpload from './ImageUpload';
import { theme } from "../../constants/loginconstant";
import {  Block, Input} from "../../components/logincomponents";
import {AddPostForm} from './AddPostForm.tsx';
import MapsLocation from '../noUsedScreen/MapsLocation'
class Add extends Component {
  state = {
    addedPost : {
      title: "",
      description: "",
      reward:false,
      category: 1,
      city: "",
      type: "",
      image: null,
      location: null
  }
  
  };
  static navigationOptions = {
    header: null,
  };

  defaultScrollViewProps = {
    keyboardShouldPersistTaps: "handled",
    contentContainerStyle: {
      flex: 1,
      justifyContent: "center",
    },
  };
  componentDidUpdate(prevProps){
    //9dim / actual jdida
    if (!prevProps.saved && this.props.saved) this.showToast();
  }
  showToast() {
    // do something
    Popup.show({ 
        type: 'Success', 
        title: 'Upload complete',
        button: false,
        textBody: 'Congrats! Your upload successfully done', 
        buttontext: 'Ok',
        autoClose: true, 
        timing: 10000,

      })
  }
  onNextStep = () => {
    console.log("called next step");
  };

  onPaymentStepComplete = () => {
    console.log("Payment step completed!");
  };

  onPrevStep = () => {
    console.log("called previous step");
  };


  onSubmitSteps = () => {
    const { type, title, category, image, description, location,reward, city } = this.state.addedPost;
    this.props.addPost({ type, title, category, image, description, location,reward, city });
  };

  render() {
    console.log(this.state.addedPost);
    
    return (
        <Root>

      <View style={{ flex: 1, marginTop: 20 , backgroundColor:"#fff"}} >
        <ProgressSteps>
          <ProgressStep
            label="Details"
            onNext={this.onPaymentStepComplete}
            onPrevious={this.onPrevStep}
            scrollViewProps={this.defaultScrollViewProps}
          >
            <AddPostForm
            // addedPost={this.state.addedPost}
            setAddedPost={(addedPost) => this.setState({ addedPost: {
              ...this.state.addedPost,
              ...addedPost
            } })}
            
            />
          </ProgressStep>

          <ProgressStep
            label="Location"
            onNext={this.onNextStep}
            onPrevious={this.onPrevStep}
            scrollViewProps={this.defaultScrollViewProps}
          >
            <View style={{ flex: 1 }}>
               <Map
                onLocationChange={(location) => this.setState({ location })}
                onLocationChange={(location) => this.setState({ addedPost: {
                  ...this.state.addedPost,
                  location  
                }
                 })}
                location={this.state.addedPost.location}
              /> 
              {/* <MapsLocation/> */}
            </View>
          </ProgressStep>
          <ProgressStep
            label="Upload"
            onPrevious={this.onPrevStep}
            onSubmit={this.onSubmitSteps}
            scrollViewProps={this.defaultScrollViewProps}
          >
            <View style={{ alignItems: "center" }}>
              <ImageUpload
                onImageUpload={(image) => this.setState({ addedPost: {
                  ...this.state.addedPost,
                  image  
                }
                 })}
                image={this.state.image}
              />
            </View>
          </ProgressStep>
        </ProgressSteps>
      </View>
      </Root>

    );
  }
}
const actions = { addPost };
function mapStateToProps(state) {
  return {
    //currentPost: state.post.newPost
    error: state.post.error,
    loading: state.post.loading,
    saved: state.post.saved

  };
}
export default connect(mapStateToProps, actions)(Add);

const styles = StyleSheet.create({
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: 'green',
    height: 40,
    borderRadius: 5,
    margin: 10,
  },
  SectionStyle2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    //borderWidth: 0.5,
   // borderColor: "#12e3be",
   height: 50,
    //borderRadius: 5,
    margin: 10,
  },
  SectionStyle3: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
   // borderWidth: 0.5,
    //borderColor: "#12e3be",
   height: 80,
   
   // borderRadius: 5,
    margin: 10,
  },input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  });