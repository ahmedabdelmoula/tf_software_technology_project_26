const city = [
{text:"Ariana",value:"Ariana"},
{text:"Sfax",value:"Sfax"},
{text:"Beja",value:"Beja"},
{text:"Ben Arous",value:"BenArous"},
{text:"Bizete",value:"Bizete"},
{text:"Gabes",value:"Gabes"},
{text:"Gafsa",value:"Gafsa"},
{text:"Jandouba",value:"Jandouba"},
{text:"Kairouan",value:"Kairouan"},
{text:"Kaserine",value:"Kaserine"},
{text:"Kef",value:"Kef"},
{text:"Mahdia",value:"Mahdia"},
{text:"Manouba",value:"Manouba"},
{text:"Mednine",value:"Mednine"},
{text:"Monastir",value:"Monastir"},
{text:"Nabeul",value:"Nabeul"},
{text:"Siliana",value:"Siliana"},
{text:"Sousse",value:"Sousse"},
{text:"Tataouine",value:"Tataouine"},
{text:"Tozeur",value:"Tozeur"},
{text:"Tunis",value:"Tunis"},
{text:"Zaghouan",value:"Zaghouan"}
]


export { city}

