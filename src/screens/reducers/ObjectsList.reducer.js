import {
    LOADING_OBJECT_FAILED,LOADING_OBJECT_SUCCESS,LOADING_OBJECTS} from '../actions/types';
  
  const INITIAL_STATE = { loading: false, error: '', objects: [] }
  
  export default(state = INITIAL_STATE, action) => {
    switch(action.type) {
      case LOADING_OBJECTS:
        return { ...state, loading: true }
      case LOADING_OBJECT_SUCCESS:
        return { ...state, loading: false, objects: action.objects}
      case LOADING_OBJECT_FAILED:
        return { ...state, loading: false, error: action.error }
      default:
        return state;
    }
  }