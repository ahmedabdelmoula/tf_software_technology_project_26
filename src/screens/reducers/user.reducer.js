import {
  LOADING_PROFILE_SUCCESS,
  LOADING_PROFILE_FAILED,
  LOADING_PROFILE,UPDATE_USER,SAVE_BOOK_MARKS ,DELETE_BOOK_MARKS
  } from '../actions/types';

const INITIAL_STATE = { loading: false, error: '', user:{} }

export default(state = INITIAL_STATE, action) => {
  switch(action.type) {
    case UPDATE_USER :
      return { ...state, user:action.user }
    case  LOADING_PROFILE:
      return { ...state, loading: true }
    case SAVE_BOOK_MARKS :
      let favoritePost = action.favoritePost;
      return{
        ...state, user :{ ...state.user , bookmarks :[ ...state.user.bookmarks,favoritePost]} 
       }
       case DELETE_BOOK_MARKS :
        let favoritePostDeleted = action.favoritePostDeleted;

         var unsavedBookmark = [];
         unsavedBookmark= state.user.bookmarks.filter(bookmark => bookmark !==favoritePostDeleted  )

        return{
          ...state, user : {
            ...state.user,bookmarks :unsavedBookmark
          }
         }
    case LOADING_PROFILE_SUCCESS:
      return { ...state, loading: false, user: action.user}
    case LOADING_PROFILE_FAILED:
      return { ...state, loading: false, error: action.error }
    default:
      return state;
  }
}