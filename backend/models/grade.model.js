const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var autoIncrement = require("mongoose-auto-increment");
const GradeSchema = new Schema(
  {
    gradeName: { type: String },
  
    highPoint: {
        type: Number
      },
      lowPoint: {
        type: Number
      },benefices : [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Benefice"
        }
      ]
  },
  {
    _id: false ,
    timestamps: true
  }
);

autoIncrement.initialize(mongoose.connection);
GradeSchema.plugin(autoIncrement.plugin, "Grade");
var Grade = mongoose.model("Grade", GradeSchema);
module.exports = Grade;
