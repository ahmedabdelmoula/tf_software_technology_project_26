const mongoose = require("mongoose");
require("mongoose-double")(mongoose);
var SchemaTypes = mongoose.Schema.Types;

const PostSchema = new mongoose.Schema({
  type: {
    type: String,
  },
  title: {
    type: String,

    trim: true,
  },
  category: {
    type: Number,
  },
  likes_number: {
    type: Number,
    default: 0,
  },
  comments_number: {
    type: Number,
    default: 0,
  },
  publish_status: {
    type: Boolean,
    default: false,
  },
  description: {
    type: String,
    required: true,
    trim: true,
  },
  city: { type: String },
  image: {
    type: String,
  },
  location: {
    longitudeDelta: { type: SchemaTypes.Double },
    longitude: { type: SchemaTypes.Double },
    latitudeDelta: { type: SchemaTypes.Double },
    latitude: { type: SchemaTypes.Double },
  },
  reward: {
    type: Boolean,
    default: false,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
  comments: [
    {
      text: String,
      created: { type: Date, default: Date.now },
      postedBy: { type: mongoose.Schema.ObjectId, ref: "User" },
    },
  ],
  likes: [{ type: mongoose.Schema.ObjectId, ref: "User" }],
  createdAt: {
    type: Date,
    default: Date.now,
  },  reclamations: [
    {
      keyword: String,
      message:String,
      author: { type: mongoose.Schema.ObjectId, ref: "User" }
    }
  ],
});

// PostSchema.post('save', async () => {
//   // Increment user score.
//   const user = await mongoose.models.User.findOne({ _id: this.author })
//   user.score+=25
//   let  score = user.score;
//   switch (true) {
//       case (score < 100): user.grade= 0; break;
//       case (score >= 100 && score < 500): user.grade= 1;  break;
//       case (score >= 500 && score < 1000):user.grade= 2; break;
//       case (score >= 1000 && score < 2000):user.grade= 3;break;
//       case (score >= 2000 && score < 5000):user.grade= 4;break;
//       default: break;
//   }
//   return user.save();
// }) 
const Post = mongoose.model("Post", PostSchema);

module.exports = Post;
