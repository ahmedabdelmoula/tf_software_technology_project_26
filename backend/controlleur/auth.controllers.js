const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user.model');

//mrigla
let registerUser = (first_name, last_name, email, password,avatar) => {
	return new Promise((res, rej) => {
		if (!first_name || !last_name || !email || !password) {
			return rej('You must send all details.');
		}
		return User.find({ email: email })
			.then(user => {
				if (user.length > 0) {
					return rej('A user with that email already exists.');
				}
				let passwordHash = bcrypt.hashSync(password.trim(), 12);
				let newUser = { first_name, last_name, email ,avatar};
				newUser.password = passwordHash;
				console.log(newUser)

				res(User.create(newUser));
			})
			.catch(err => {
				rej(err);
			});
	});
};
//mrigla
let loginUser = (email, password) => {
	return new Promise((res1, rej1) => {
		if (!email || !password) {
			return rej1('You must send the username and the password.');
		}
		return User.findOne({ email: email })
			.then(user => {
				if (!user) return rej1('No matching user.');
				return new Promise((res2, rej2) => {
					bcrypt.compare(password, user.password, (err, success) => {
						if (err) {
							return rej2(
								'The has been an unexpected error, please try again later'
							);
						}
						if (!success) {
							return rej2('Your password is incorrect.');
						} else {
							res1(user);
						}
					});
				});
			})
			.catch(err => {
				rej1(err);
			});
	});
};
//mrigla
let createToken = user => {
	//return jwt.sign(_.omit(user.toObject(), 'password'), process.env.JWT_SECRET, {
	return jwt.sign({ _id: user.id.toString(), first_name: user.first_name, last_name: user.last_name },process.env.JWT_SECRET, {

		expiresIn: '604800s' //lower value for testing+
	});
};



 let createRefreshToken = user => {
	//It doesn't always need to be in the /login endpoint route
	let refreshToken = jwt.sign({ type: 'refresh' }, process.env.JWT_SECRET, {
		expiresIn: '20s' // 1 hour
	});
	return User.findOneAndUpdate(
		{ email: user.email },
		{ refreshToken: refreshToken }
	)
		.then(() => {
			return refreshToken;
		})
		.catch(err => {
			throw err;
		});
};




module.exports = {
	registerUser,
	loginUser,
	createToken
};
