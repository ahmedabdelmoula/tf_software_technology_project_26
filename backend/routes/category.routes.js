const express       = require('express');
const router = express.Router();
const Category          = require('../models/category.model')
const  authenticate = require('../middleware/auth')





router.post('/category/create',authenticate, (req,res) => {
	
    console.log(req.body)
        var category=  new Category({
        
            ...req.body
        })
        
        category.save()
        return res.send({
            success: true,
            message: 'Category Aded ',
            category:category
        });
    
        
         
    });


    router.get('/category/list', authenticate,async (req,res) => {

        await Category.find({})
        .exec((err, result) => {
            if (err) {
              console.log(err)
              return res.status(400).json({
              err
              })
            }
            res.json({
                success: true,
                message: 'List Category',
                category:result,
                length:result.length

            })
          }) 
        
    
        });

        router.get("/category/datalist/data",authenticate,  async (req,res) => {
            const posts = await Category.find({})
            var query = JSON.parse(req.query.response);
            
            let page=query.page
            let perPage=query.perPage
        
             
            let totalPages = Math.ceil(posts.length / perPage)
        
            if (page !== undefined && perPage !== undefined) {
        
              let calculatedPage = (page - 1) * perPage
              let calculatedPerPage = page * perPage
        
              res.status(200).json({
                data: posts.slice(calculatedPage, calculatedPerPage), 
                totalPages
                });
           
            } else {
        
        
               return res.status(200).json({
                    data: posts.slice(0, 4), 
                    totalPages: Math.ceil(posts.length / 4)
                    }); 
           
            }
          })
          router.patch('/category/update/:id',authenticate, async (req, res) => {
           
            Category.findByIdAndUpdate(req.params.id, {$set: {...req.body}})
            .then(() => res.json('Category updated!'))
            .catch(err => res.status(400).json('Error: ' + err));
              
            
        })

        router.delete('/category/delete/:id', authenticate,async (req,res) => {
            const _id = req.params.id
   
            try {
                const deletepost = await Category.findOneAndDelete({_id:_id})
                if (!deletepost) {
                    return res.status(404).send();
                }
                res.send(deletepost)
            } catch (error) {
                res.status(500).send()
            }
        })
    module.exports = router