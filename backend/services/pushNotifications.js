const { Expo } = require ('expo-server-sdk');
const expo = new Expo();

exports.sendNotification = async function sendNotification(message, pushToken) {
  if (!Expo.isExpoPushToken(pushToken)) throw new Error(`Invalid pushToken ${pushToken}`);

  return expo.sendPushNotificationsAsync([{ ...message, to: pushToken }]);
}
